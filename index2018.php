<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>ExpoAuto Morelia </title>		
	<meta name="description" content="Expo Auto Morelia 6ta Edicion">
	<meta name="keywords" content="Expo Auto, expo auto morelia, 2014, 2015, 2016, 2017, 2018, expo auto 2017, modelos 2017, Morelia, Michoacan, Las mejores marcas, Expo Auto Morelia, Qualitas, qualitas morelia, Exposicion de autos, Auto, Automoviles, Autos Morelia, Feria Automotriz, Toyota, BMW, Cadillac, Nissan, Volkswagen, GMC, Honda, MINI, Ford, Chrysler, Ram, Isuzu, Suzuki, Chevrolet, Buick, Altozano, Mirador Altozano, Grupo FAME, expoauto, expoauto morelia, expo altozano, exposicion altozano, expo morelia, exposicion altozano, coches altozano, carros altozano, exposicion autos, exposicion autos altozano, nissan morelia, nissan fame, nissan acueducto, vw fame, vw la huerta, vw morelia, fame manantiales, fame talisman, gmc morelia, gmc altozano, honda morelia, honda fame, honda camelinas, honda altozano, honda monarca, honda acueducto, ford morelia, volvo morelia, audi morelia, manejo expoauto, pruebas expoauto, chrysler morelia, ram morelia, isuzu morelia, camiones morelia, suzuki morelia, chevrolet morelia, chevrolet fame, buick morelia, buick manantiales, toyota morelia, toyota valladolid, toyota fame, autos nuevos, coches nuevos, camionetas morelia, motos morelia, seminuevos morelia."/>     
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Google Font -->
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	
	<link rel="shortcut icon" href="img/favicon.png" />

	<link rel="stylesheet" href="css/fontawesome.min.css">
	<link rel="stylesheet" href="css/fontawesome.css">
	<link rel="stylesheet" href="css/all.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery.fancybox.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/media-queries.css">

	<script src="js/modernizr-2.6.2.min.js"></script>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-54722313-1', 'auto');
		ga('send', 'pageview');

	</script>

</head>

<body id="body">

	

	<header id="navigation" class="navbar-fixed-top navbar">
		<div class="container">
			<div class="navbar-header">

				<a class="navbar-brand" href="#body">
					<h1 id="logo">
						<img src="img/logo.png" alt="expo">
					</h1>
				</a>
				
			</div>

			<nav class="collapse navbar-collapse navbar-right" role="navigation">
				<ul id="nav" class="nav navbar-nav">
					<li><a href="#registro"><strong> REGISTRO</strong></a></li>
					<li class="current"><a href="#body">Inicio</a></li>
					<li><a href="#ubicacion">Ubicacion</a></li>
					<li><a href="#galeria">Galería</a></li>
					<li><a href="#marcas">Marcas</a></li>
					<li><a href="#facts">Estadísticas</a></li>
				</ul>
			</nav>
			
		</div>
	</header>
	
	<section id="slider">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				<div class="item active" style="background-image: url(img/banner.jpg);">
					
					<div class="carousel-caption">
						<br>
						<br>
						
						<div>
							<h3 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated">
								<img src="img/logoexpo.png" alt=""> </h3>
							</div>	
							<br>
							<h4 data-wow-duration="1000ms" class="wow slideInLeft animated" >El mejor Evento Automotriz de la Región. </h4>
							<br>
							<h4 data-wow-duration="1000ms" class="wow slideInRight animated"><FONT  SIZE=10 > <strong>9, 10 y 11 de Noviembre</strong> </FONT> </h4>
							<br>
							<br><br>
							<ul class="social-links text-center">
								<li><a href="https://www.facebook.com/expoautomorelia" target="_blank"><i class="fab fa-facebook fa-4x"></i></a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<li><a href="https://twitter.com/expoautomorelia" target="_blank"><i class="fab fa-twitter fa-4x"></i></a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<li><a href="https://www.instagram.com/expoautomorelia/" target="_blank"><i class="fab fa-instagram fa-4x"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="ubicacion" class="ubicacion">
			<div id="map_canvas" class="wow bounceInDown animated" data-wow-duration="500ms"></div>
		</section>

		<section id="registro" class="contact">
			<div class="container">
				<div class="row mb50">
					<br>
					<div class="sec-title text-center mb50 wow fadeInDown animated" data-wow-duration="500ms">
						<h2>REGISTRO</h2>
						<div class="devider"><i class="fa fa-car fa-3x"></i></div>
					</div>

					<div class="sec-sub-title text-center wow rubberBand animated" data-wow-duration="1000ms">
						<p>Regístrate para apartar tu lugar.</p>
					</div>

					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 wow fadeInLeft animated" data-wow-duration="500ms">
						<div class="contact-address"><center>
							<h3>Se llevará a cabo en:</h3>
							<p>Plaza Comercial Altozano</p>

							<IMG src="img/altozano.png" width="180" height="120"/>
						</center>

					</div>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">

					<script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
					<script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.8183" type="text/javascript"></script>
					<script type="text/javascript">
						JotForm.init(function(){
							JotForm.alterTexts(undefined);
							JotForm.clearFieldOnHide="disable";
							JotForm.submitError="jumpToFirstError";
							/*INIT-END*/
						});

						JotForm.prepareCalculationsOnTheFly([null,{"name":"expoAuto","qid":"1","text":"Expo auto Morelia 2018","type":"control_head"},{"name":"submit2","qid":"2","text":"Enviar","type":"control_button"},{"description":"","name":"nombreCompleto3","qid":"3","subLabel":"","text":"Nombre completo","type":"control_textbox"},{"description":"","name":"whatsapp10","qid":"4","subLabel":"","text":"Whatsapp (10 digitos)","type":"control_textbox"},{"description":"","name":"correoElectronico","qid":"5","subLabel":"","text":"Correo Electronico","type":"control_textbox"},null,null,{"description":"","name":"fechaDe","qid":"8","subLabel":"","text":"Fecha de asistencia:","type":"control_dropdown"},{"description":"","name":"marcaDe9","qid":"9","text":"Marca de interes (Puede seleccionar 3 marcas):","type":"control_checkbox"},{"name":"clicPara","qid":"10","text":"Con Expo Auto tus datos estan seguros. Al dar clic al boton enviar, autorizas el tratamiento de tus datos personales para contacto y\u002Fo acciones de mercadotecnia , y declaras haber leido nuestro aviso de privacidad disponible aqui.","type":"control_text"}]);
						setTimeout(function() {
							JotForm.paymentExtrasOnTheFly([null,{"name":"expoAuto","qid":"1","text":"Expo auto Morelia 2018","type":"control_head"},{"name":"submit2","qid":"2","text":"Enviar","type":"control_button"},{"description":"","name":"nombreCompleto3","qid":"3","subLabel":"","text":"Nombre completo","type":"control_textbox"},{"description":"","name":"whatsapp10","qid":"4","subLabel":"","text":"Whatsapp (10 digitos)","type":"control_textbox"},{"description":"","name":"correoElectronico","qid":"5","subLabel":"","text":"Correo Electronico","type":"control_textbox"},null,null,{"description":"","name":"fechaDe","qid":"8","subLabel":"","text":"Fecha de asistencia:","type":"control_dropdown"},{"description":"","name":"marcaDe9","qid":"9","text":"Marca de interes (Puede seleccionar 3 marcas):","type":"control_checkbox"},{"name":"clicPara","qid":"10","text":"Con Expo Auto tus datos estan seguros. Al dar clic al boton enviar, autorizas el tratamiento de tus datos personales para contacto y\u002Fo acciones de mercadotecnia , y declaras haber leido nuestro aviso de privacidad disponible aqui.","type":"control_text"}]);}, 20); 
						</script>

						<div class="contact-form">
							<h3>Registra tus datos</h3>

							<form id="contact-form" class="jotform-form" action="https://submit.jotform.co/submit/83017104930851/" method="post" name="form_83017104930851" id="83017104930851" accept-charset="utf-8">

								<div class="input-group nombre-apellido">
									<div class="input-field"> 
										<input type="text" id="input_3" name="q3_nombreCompleto3" data-type="input-textbox"  placeholder="Nombre Completo" class="form-control validate[required]" size="20" value="" data-component="textbox" required="" />
										<?php if(isset($errors)){ echo $errors[1]; } ?>
									</div>
									<div class="input-field">
										<input type="text" id="input_4" name="q4_whatsapp10" data-type="input-textbox" class="form-control validate[required, Numeric]" size="10" value="" maxLength="10" data-component="textbox" placeholder="WhatsApp" required="" />
										<?php if(isset($errors)){ echo $errors[2]; } ?>
									</div>
								</div>

								<div class="input-group telefono-mail">
									<div class="input-field">
										<input type="text" id="input_5" name="q5_correoElectronico" placeholder="Correo Electronico" class="form-control"  class="form-control">
										<?php if(isset($errors)){ echo $errors[3]; } ?>
									</div>
									<div class="input-field">
										<select class="form-control validate[required]" id="input_8" name="q8_fechaDe" style="width:250px" data-component="dropdown" required="">
											<option value="" selected>Fecha de asistencia.</option>
											<option value="Viernes 9 de Noviembre"> Viernes 9 de Noviembre </option>
											<option value="Sábado 10 de Noviembre"> Sábado 10 de Noviembre </option>
											<option value="Domingo 11 de Noviembre"> Domingo 11 de Noviembre </option>
										</select>
										<?php if(isset($errors)){ echo $errors[4]; } ?>
									</div>
								</div>

								<div class="input-group seleccion">

									<label class="form-label form-label-top form-label-auto" id="label_9" for="input_9_0">
										Marca de interés ( selecciona 3): <?php if(isset($errors)){ echo $errors[5]; } ?>
									</label>
									<div id="cid_9" name="cid_9" class="form-control jf-required">
										<div class="form-single-column" data-component="checkbox">
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_0" name="q9_marcaDe9[]" value="BMW" required="" data-maxselection="3" />
												<label id="label_input_9_0" for="input_9_0"> BMW </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_1" name="q9_marcaDe9[]" value="Chevrolet" required="" data-maxselection="3" />
												<label id="label_input_9_1" for="input_9_1"> Chevrolet </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_2" name="q9_marcaDe9[]" value="GMC" required="" data-maxselection="3" />
												<label id="label_input_9_2" for="input_9_2"> GMC </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_3" name="q9_marcaDe9[]" value="Honda" required="" data-maxselection="3" />
												<label id="label_input_9_3" for="input_9_3"> Honda </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_4" name="q9_marcaDe9[]" value="Isuzu" required="" data-maxselection="3" />
												<label id="label_input_9_4" for="input_9_4"> Isuzu </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_5" name="q9_marcaDe9[]" value="Kia" required="" data-maxselection="3" />
												<label id="label_input_9_5" for="input_9_5"> Kia </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_6" name="q9_marcaDe9[]" value="Mini" required="" data-maxselection="3" />
												<label id="label_input_9_6" for="input_9_6"> Mini </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_7" name="q9_marcaDe9[]" value="Nissan" required="" data-maxselection="3" />
												<label id="label_input_9_7" for="input_9_7"> Nissan </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_8" name="q9_marcaDe9[]" value="Toyota" required="" data-maxselection="3" />
												<label id="label_input_9_8" for="input_9_8"> Toyota </label>
											</span>
											<span class="form-checkbox-item" style="clear:left">
												<span class="dragger-item">
												</span>
												<input type="checkbox" class="form-checkbox validate[required, maxselection]" id="input_9_9" name="q9_marcaDe9[]" value="Volkswagen" required="" data-maxselection="3" />
												<label id="label_input_9_9" for="input_9_9"> Volkswagen </label>
											</span>
										</div>
									</div>
								</li>
							</div>

							<div class="input-group">
								<input type="submit" name='boton' id="input_2" class="pull-right" value="ENVIAR" data-component="button">
								<?php if(isset($result)) { echo $result; } ?>
							</div>

							<div id="cid_10" class="">
								<div id="text_10" class="" data-component="text">
									<p>Con Expo Auto tus datos están seguros. Al dar clic al botón enviar, autorizas el tratamiento de tus datos personales para contacto y/o acciones de mercadotecnia , y declaras haber leído nuestro aviso de privacidad disponible <a href="http://expoautomorelia.com/aviso.html" rel="nofollow" target="_blank"> <font color="blue"> aquí </font> </a>.</p>
								</div>
							</div>

							<script>
								JotForm.showJotFormPowered = "0";
							</script>
							<input type="hidden" id="simple_spc" name="simple_spc" value="83017104930851" />
							<script type="text/javascript">
								document.getElementById("si" + "mple" + "_spc").value = "83017104930851-83017104930851";
							</script>

						</form> 

						<script type="text/javascript">JotForm.ownerView=true;</script>
						
					</div>  
				</div>

				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 wow fadeInRight animated" data-wow-duration="500ms" data-wow-delay="600ms">
					<ul class="footer-social">
						<li><a href="https://www.facebook.com/expoautomorelia" target="_blank"><i class="fab fa-facebook fa-2x"></i></a></li>
						<li><a href="https://twitter.com/expoautomorelia" target="_blank"><i class="fab fa-twitter fa-2x"></i></a></li>
						<li><a href="https://www.instagram.com/expoautomorelia/" target="_blank"><i class="fab fa-instagram fa-2x"></i></a></li>

					</ul>
				</div>
			</div>
		</div>
	</section>

	<section id="marcas" class="team">
		<div class="container">
			<div class="row">
				<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
					<h2>Marcas Participantes</h2>
					<div class="devider"><i class="fa fa-car fa-3x"></i></div>
				</div>
				<div class="sec-sub-title text-center wow fadeInRight animated" data-wow-duration="500ms">
					<p>  </p>
				</div>

				<!-- BMW ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/bmw.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>BMW</h5>
							<p>Talisman</p>
							<ul class="social-links text-center">
								<li><a href="http://www.fametalisman.com/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/bmwmorelia/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/BMWFame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>BMW</h4>
					<span>Talisman</span>
				</figure>

				<!-- BUICK ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
					<div class="member-thumb">
						<img src="img/team/buick.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>BUICK</h5>
							<p>Camelinas</p>
							<ul class="social-links text-center">
								<li><a href="http://www.famemanantiales.com/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/famemanantiales/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/manantialesf" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>BUICK</h4>
					<span>Camelinas</span>
				</figure>

				<!-- CADILLAC============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
					<div class="member-thumb">
						<img src="img/team/cadillac.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>CADILLAC</h5>
							<p>Camelinas</p>
							<ul class="social-links text-center">
								<li><a href="http://www.famemanantiales.com/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/famemanantiales/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/manantialesf" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>CADILLAC</h4>
					<span>Camelinas</span>
				</figure>

				<!-- CHEVROLET ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
					<div class="member-thumb">
						<img src="img/team/chevrolet.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>CHEVROLET</h5>
							<p>Morelia</p>
							<ul class="social-links text-center">
								<li><a href="http://www.famechevrolet.com.mx/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/chevroletFame/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/chevroletfame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>CHEVROLET</h4>
					<span>Morelia</span>
				</figure>

				<!-- GMC ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
					<div class="member-thumb">
						<img src="img/team/gmc.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>GMC</h5>
							<p>Camelinas</p>
							<ul class="social-links text-center">
								<li><a href="http://www.famemanantiales.com/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/famemanantiales/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/manantialesf" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>GMC</h4>
					<span>Camelinas</span>
				</figure>

				<!-- HONDA ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/honda.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>HONDA</h5>
							<p>Morelia</p>
							<ul class="social-links text-center">
								<li><a href="http://www.hondamorelia.com/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/grupofame/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/HondaFame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>Honda</h4>
					<span>Morelia</span>
				</figure>

				<!-- ISUZU ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/isuzu.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>ISUZU</h5>
							<p>Manantiales</p>
							<ul class="social-links text-center">
								<li><a href="http://famecamiones.com/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/Isuzu-Fame-1554203564847510/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/grupofame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>ISUZU</h4>
					<span>Manantiales</span>
				</figure>

				<!-- KIA ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/kia.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>KIA</h5>
							<p>Mil Cumbres</p>
							<ul class="social-links text-center">
								<li><a href="http://dealers.kia.com/mx/milcumbres/main.html" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/kiamilcumbres/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/kia_fame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>KIA</h4>
					<span>Mil Cumbres</span>
				</figure>

				<!-- MINI ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/mini.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>MINI</h5>
							<p>Talisman</p>
							<ul class="social-links text-center">
								<li><a href="http://www.fametalisman.com/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/MINI-Fame-Talism%C3%A1n-458847920889256/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/grupofame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>MINI</h4>
					<span>Talisman</span>
				</figure>

				<!-- MOTORRAD ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/motorrad.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>BMW Motorrad</h5>
							<p>Morelia</p>
							<ul class="social-links text-center">
								<li><a href="http://www.motorradmorelia.com/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/BMW-Motorrad-Morelia-1390899507623194/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/grupofame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>BMW Motorrad</h4>
					<span>Morelia</span>
				</figure>

				<!-- ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/nissan.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>NISSAN</h5>
							<p>Acueducto</p>
							<ul class="social-links text-center">
								<li><a href="http://www.nissanacueducto.com.mx/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/NissanAcueductoM/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/NissanAcueducto" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>NISSAN</h4>
					<span>Acueducto</span>
				</figure>

				<!-- ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/toyota.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>TOYOTA</h5>
							<p>Altozano</p>
							<ul class="social-links text-center">
								<li><a href="http://www.famevalladolid.com" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/toyotaaltozano/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/ToyotaFame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>TOYOTA</h4>
					<span>Altozano</span>
				</figure>

				<!-- ============================================================================= -->
				<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					<div class="member-thumb">
						<img src="img/team/vw.png" alt="Team Member" class="img-responsive">
						<figcaption class="overlay">
							<h5>VOLKSWAGEN</h5>
							<p>Morelia</p>
							<ul class="social-links text-center">
								<li><a href="http://www.vw-fame.com.mx/" target="_blank"><i class="fas fa-globe fa-lg"></i></a></li>
								<li><a href="https://www.facebook.com/vwfame/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>
								<li><a href="https://twitter.com/VolkswagenFame" target="_blank"><i class="fab fa-twitter-square fa-lg"></i></a></li>
							</ul>
						</figcaption>
					</div>
					<h4>VOLKSWAGEN</h4>
					<span>Morelia</span>
				</figure>

				<!-- ============================================================================= -->

			</div>
		</div>
	</section>

	<section id="galeria" class="works clearfix">
		<div class="container">
			<div class="row">

				<div class="sec-title text-center">
					<h2>Ediciones Pasadas</h2>
					<div class="devider"><i class="fa fa-car fa-3x"></i></div>
				</div>

				<div class="sec-sub-title text-center">
					<p>Imágenes de las ediciones pasadas del mejor evento automotriz de la región.</p>
				</div>

			</div>
		</div>

		<div class="project-wrapper">

			<figure class="mix work-item web">
				<img src="img/works/02.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="2" href="img/works/02.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item logo-design">
				<img src="img/works/03.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="3" href="img/works/03.jpg"><i class="fa fa-eye fa-lg"></i></a>

				</figcaption>
			</figure>


			<figure class="mix work-item branding">
				<img src="img/works/05.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="5" href="img/works/05.jpg"><i class="fa fa-eye fa-lg"></i></a>

				</figcaption>
			</figure>

			<figure class="mix work-item web">
				<img src="img/works/06.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="6" href="img/works/06.jpg"><i class="fa fa-eye fa-lg"></i></a>

				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/08.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/08.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/09.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/09.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/10.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/10.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/11.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/11.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/12.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/12.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/13.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/13.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/14.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/14.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/17.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/08.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/18.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/18.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/19.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/19.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/20.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/20.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/22.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/22.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/23.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/23.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/24.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/24.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/26.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/26.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/27.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/27.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/28.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/28.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/29.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/29.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/30.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/30.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/31.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/31.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/32.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/32.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/33.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/33.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/34.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/34.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/35.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/35.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/36.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/36.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/37.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/37.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/38.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/38.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>

			<figure class="mix work-item photography">
				<img src="img/works/39.jpg" alt="">
				<figcaption class="overlay">
					<a class="fancybox" rel="works" title="8" href="img/works/39.jpg"><i class="fa fa-eye fa-lg"></i></a>
				</figcaption>
			</figure>
		</div>
	</section>

	<?php
	if (isset($_POST['boton'])) {
		if($_POST['nombre'] == '') {
			$errors[1] = '<span class="error"> </span>';
		} else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
			$errors[2] = '<span class="error"> </span>';
		} else if($_POST['telefono'] == '') {
			$errors[3] = '<span class="error"> </span>';
		} else if($_POST['mensaje'] == '') {
			$errors[4] = '<span class="error"> </span>';
		} else if($_POST['apellido'] == '') {
			$errors[5] = '<span class="error">  </span>';
		} else if($_POST['edad'] == '') {
			$errors[6] = '<span class="error"> </span>';
		} else if($_POST['genero'] == '') {
			$errors[7] = '<span class="error"> </span>';
		} else if($_POST['marca'] == '') {
			$errors[8] = '<span class="error"> </span>';
		} else {
			$dest = "contacto@expoautomorelia.com" ;
			$nombre = $_POST['nombre'];
			$apellido = $_POST['apellido'];
			$email = $_POST['email'];
			$telefono = $_POST['telefono'];
			$edad = $_POST['edad'];
			$genero = $_POST['genero'];
			$marca = $_POST['marca'];
			$asunto_cte = "Preregistro Contacto Expo Auto Morelia 2018";
			$asunto = "Preregistro Contacto Expo Auto Morelia 2018";
			$cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "&nbsp;" . $apellido ."<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'] ."<br>" . "Mi edad es: " . $_POST['edad'] ."<br>" . "Mi genero es: " . $_POST['genero'] ."<br>" . "Mi marca de interes es: " . $_POST['marca'] ."<br>" ;
			$cuerpo_cte = '
			<html>
			<head>
			<title>Mail from '. $nombre .'</title>
			</head>
			<body>
			<table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
			<tr style="height: 32px;">
			<th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
			<td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
			</tr>
			<tr style="height: 32px;">
			<th align="right" style="width:150px; padding-right:5px;">Apellido:</th>
			<td align="left" style="padding-left:5px; line-height: 20px;">'. $apellido .'</td>
			</tr>
			<tr style="height: 32px;">
			<th align="right" style="width:150px; padding-right:5px;">Correo:</th>
			<td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
			</tr>
			<tr style="height: 32px;">
			<th align="right" style="width:150px; padding-right:5px;">Edad:</th>
			<td align="left" style="padding-left:5px; line-height: 20px;">'. $edad .'</td>
			</tr>
			<tr style="height: 32px;">
			<th align="right" style="width:150px; padding-right:5px;">Telefono:</th>
			<td align="left" style="padding-left:5px; line-height: 20px;">'. $telefono .'</td>
			</tr>
			<tr style="height: 32px;">
			<th align="right" style="width:150px; padding-right:5px;">Genero:</th>
			<td align="left" style="padding-left:5px; line-height: 20px;">'. $genero .'</td>
			</tr>
			<tr style="height: 32px;">
			<th align="right" style="width:150px; padding-right:5px;">Marca:</th>
			<td align="left" style="padding-left:5px; line-height: 20px;">'. $marca .'</td>
			</tr>

			<tr style="height: 32px;">
			<th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
			<td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
			</tr>
			</table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";

			if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
				$result = '<div class="result_ok">Email enviado correctamente</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
				$_POST['nombre'] = '';
				$_POST['email'] = '';
				$_POST['telefono'] = '';
				$_POST['mensaje'] = '';
			} else {
				$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
			}
		}
	}
	?>

	<footer id="footer" class="footer">
		<br>
		<p  align="center">
			<img src="img/footer-logo.png" alt=""> <br>
			<a href="http://grupofame.com/">Expo Auto Morelia © 2018</a>.  <a href="aviso.html" target="_blank"> Aviso de privacidad. </a>
		</p>
		<br>
	</footer>

	<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/jquery.singlePageNav.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.parallax-1.1.3.js"></script>
	<script src="js/jquery-countTo.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyB6gh9dzOYIWa8a2si2shYQXFJzzE-4wdw'></script>
	<script src="js/jquery.easing.min.js"></script>->
	<script src="js/wow.min.js"></script>
	<script>
		var wow = new WOW ({
			boxClass:     'wow',      
			animateClass: 'animated', 
			offset:       120,          
			mobile:       false,       
			live:         true        
		}
		);
		wow.init();
	</script> 
	<script src="js/custom.js"></script>
	<script type="text/javascript">

		$(function(){

			$('#contact-form').validate({
				rules: {
					nombre: {
						required: true,
						minlength: 2
					},
					q3_nombreCompleto3: {
						required: true,
						minlength: 2
					},
					apellido: {
						required: true,
						minlength: 2
					},
					email: {
						required: true,
						email: true
					},
					q5_correoElectronico: {
						required: true,
						email: true
					},
					telefono: {
						required: true,
						minlength: 10
					},
					q4_whatsapp10: {
						required: true,
						minlength: 10
					},
					genero: {
						required: true,
						minlength: 2
					},
					edad: {
						required: true,
						minlength: 1
					},
					marca: {
						required: true,
					},

					q8_fechaDe: {
						required: true,
					},

					mensaje: {
						required: false,
						minlength: 5
					}
				},
				messages: {
					q3_nombreCompleto3: {
						required: "Escribe tu nombre",
						minlength: "Tu nombre debe contener mas de 2 caracteres"
					},
					apellido: {
						required: "Escribe tu apellido",
						minlength: "Tu apellido debe contener mas de 2 caracteres"
					},
					q5_correoElectronico: {
						required: "Escribe tu correo electronico",
						email: "Escribe un correo valido"
					},
					q4_whatsapp10: {
						required: "Escribe tu número de WhatsApp",
						minlength: "Debe tener al menos 10 digitos"
					},
					genero: {
						required: "Escribe tu genero",
						minlength: "Debe tener al menos 2 digitos"
					},
					edad: {
						required: "Escribe tu edad",
						minlength: "Debe tener al menos 1 digitos"
					},
					cid_9: {
						required: "Selecciona una marca",
						 
					},

					q8_fechaDe: {
						required: "Selecciona una fecha para asistir"
					}
				},

				submitHandler: function(form) {
					$(form).ajaxSubmit({
						type:"POST",
						data: $(form).serialize(),
						url:"index.php",
						success: function() {
							$('#contact-form :input').attr('disabled', 'disabled');
							$('#contact-form').fadeTo( "slow", 0.15, function() {
								$(this).find(':input').attr('disabled', 'disabled');
								$(this).find('label').css('cursor','default');
								$('#success').fadeIn();
							});
						},
						error: function() {
							$('#contact-form').fadeTo( "slow", 0.15, function() {
								$('#error').fadeIn();
							});
						}
					});
				}
			});
		});
	</script>

</body>
</html>
